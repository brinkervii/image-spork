import os
from string import Template

HERE = os.path.dirname(os.path.abspath(__file__))


def _image_spork_desktop_text() -> str:
    return """\
[Desktop Entry]
Version=1.0
Type=Application
Name=Image Spork
Icon=image
Comment=Spork some images!
Exec=$HOME/.local/bin/image-spork spork %f
Categories=Graphics;2DGraphics;RasterGraphics;
MimeType=image/bmp;image/g3fax;image/gif;image/x-fits;image/x-pcx;image/x-portable-anymap;image/x-portable-bitmap;image/x-portable-graymap;image/x-portable-pixmap;image/x-psd;image/x-sgi;image/x-tga;image/x-xbitmap;image/x-xwindowdump;image/x-xcf;image/x-compressed-xcf;image/x-gimp-gbr;image/x-gimp-pat;image/x-gimp-gih;image/tiff;image/jpeg;image/x-psp;application/postscript;image/png;image/x-icon;image/x-xpixmap;image/x-exr;image/x-webp;image/heif;image/heic;image/svg+xml;application/pdf;image/x-wmf;image/jp2;image/x-xcursor;
    """


def image_spork_desktop(home=os.environ["HOME"]):
    template = Template(_image_spork_desktop_text())

    return template.safe_substitute({
        "HOME": home
    })
