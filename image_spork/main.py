import io
import os
import shutil
import subprocess
from pathlib import Path

import click as click
from PIL import Image

from image_spork import resources


@click.group()
def cli():
    pass


def image_file_to_png_bytes(path: Path):
    with path.open("rb") as fp:
        with io.BytesIO() as png_buffer:
            with Image.open(fp=fp) as image:
                image: Image.Image = image
                image.save(fp=png_buffer, format="PNG")

                png_buffer.seek(0)
                return png_buffer.read()


@click.argument("image_location")
@cli.command("spork")
def command_spork(image_location: str):
    image_path = Path(image_location).absolute()
    screenshots_dir = Path(os.environ["HOME"], "Pictures", "Screenshots").absolute()

    if not screenshots_dir.exists():
        os.makedirs(screenshots_dir)

    try:
        shutil.copy(image_path, screenshots_dir.joinpath(image_path.name))

    except shutil.SameFileError:
        pass

    image_bytes = image_file_to_png_bytes(image_path)

    proc = subprocess.Popen(
        [
            "xclip",
            "-sel", "clip",
            "-t", "image/png",
        ],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )

    click.echo(f"Writing {len(image_bytes)} bytes to the clipboard")

    proc.stdin.write(image_bytes)
    proc.stdin.close()
    proc.wait()

    click.echo(f"XClip return code: {proc.returncode}")


def update_desktop_database(path: Path):
    subprocess.check_call(["update-desktop-database", str(path.absolute())])


@cli.command("integrate")
def command_integrate():
    applications = Path(os.environ["HOME"], ".local", "share", "applications").absolute()
    if not applications.exists():
        os.makedirs(applications)

    desktop = applications.joinpath("image-spork.desktop")

    with desktop.open("w+") as fp:
        fp.write(resources.image_spork_desktop())

    desktop.chmod(0o755)

    update_desktop_database(applications)


@cli.command("de-integrate")
def command_de_integrate():
    applications = Path(os.environ["HOME"], ".local", "share", "applications").absolute()
    if not applications.is_dir():
        return

    desktop = applications.joinpath("image-spork.desktop")

    os.remove(desktop)

    update_desktop_database(applications)


def main():
    return cli()


if __name__ == '__main__':
    main()
