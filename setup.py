import os
import site

from setuptools import setup

HERE = os.path.dirname(os.path.abspath(__file__))
site.addsitedir(HERE)

from image_spork import __version__

setup(
    name="image_spork",
    version=__version__,
    author="BrinkerVII",
    author_email="brinkervii@gmail.com",
    packages=["image_spork"],
    include_package_data=True,
    install_requires=[
        "click",
        "Pillow"
    ],
    entry_points={
        "console_scripts": [
            "image-spork=image_spork.main:main"
        ]
    }
)
