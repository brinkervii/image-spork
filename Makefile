_HOME = "${HOME}"
PREFIX ?= $(_HOME)/.local
INSTALL_BIN = $(PREFIX)/bin

install: dist/image-spork
	mkdir -p $(INSTALL_BIN)
	install -Dm755 dist/image-spork $(INSTALL_BIN)/image-spork

venv:
	virtualenv venv
	bash -c 'source ./venv/bin/activate; pip install -r requirements.txt; pip install -r requirements.dev.txt'

dist/image-spork: venv
	bash -c 'source ./venv/bin/activate; pyinstaller -y -F -n image-spork image_spork/main.py'

